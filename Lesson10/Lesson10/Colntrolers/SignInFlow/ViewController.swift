//
//  ViewController.swift
//  Lesson10
//
//  Created by Martynov Evgeny on 3.02.21.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var tokenSwich: UISwitch!
    
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passTF: UITextField!
    
    @IBOutlet weak var signInBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        if checkToken() {
            performSegue(withIdentifier: "goToMainStoryboard", sender: nil)
        }
    }
    
    @IBAction func emailTFChanged(_ sender: UITextField) {
        signInBtn.isEnabled = isEmptyTF()
    }
    
    @IBAction func passTFChanged(_ sender: UITextField) {
        signInBtn.isEnabled = isEmptyTF()
    }
    
    @IBAction func signInBtnAction() {
        if isValidEmail(emailTF.text ?? "") {
            
            //TODO: - check credantials and saveToken()
            //TODO: performSegue
            
            //saveCredantials()
            //saveToken()
            
            //проверка email + pass
            //performSegue(withIdentifier: "goToMainStoryboard", sender: nil)

        } else {
            errorLabel.isHidden = false
            errorLabel.text = "Email is not valid"
        }
    }
    
    @IBAction func unwindToSignIn(_ unwindSegue: UIStoryboardSegue) {
        //let sourceViewController = unwindSegue.source
        // Use data from the view controller which initiated the unwind segue
    }
    
    private func saveCredantials() {
        UserDefaults.standard.set(emailTF.text, forKey: "email")
        UserDefaults.standard.set(passTF.text, forKey: "pass")
    }
    
    
    private func saveToken() {
        if tokenSwich.isOn {
            let date = Date()
            UserDefaults.standard.set(date, forKey: "date")
        }
    }
    
    private func isEmptyTF() -> Bool {
        
        errorLabel.isHidden = true
        
        if let emailTFText = emailTF.text,
           let passTFText = passTF.text {
            return !emailTFText.isEmpty && !passTFText.isEmpty
        } else {
            return false
        }
    }
    
    
    private func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    private func checkToken() -> Bool {
        if let date = UserDefaults.standard.object(forKey: "date") as? Date,
           let earlyDate = Calendar.current.date(
            byAdding: .minute,
            value: -1,
            to: Date()) {
            return earlyDate <= date
        } else {
            return false
        }
    }
}

