//
//  SetPasswordVC.swift
//  Lesson10
//
//  Created by Martynov Evgeny on 8.02.21.
//

import UIKit

class SetPasswordVC: UIViewController {
    
    @IBOutlet weak var passTF: UITextField!
    @IBOutlet weak var passTF2: UITextField!
    
    @IBOutlet weak var verifView1: UIView!
    @IBOutlet weak var verifView2: UIView!
    @IBOutlet weak var verifView3: UIView!
    @IBOutlet weak var verifView4: UIView!
    
    @IBOutlet weak var finishReg: UIButton!
    
    @IBOutlet weak var switchToken: UISwitch!
    
    
    var userModel: UserModel = UserModel()
    
    var passwordLevel = 0

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func passTF1Changed(_ sender: UITextField) {
        checkPasswordStrenght()
        isPasswordEquality()
    }
    
    @IBAction func passTF2Changed(_ sender: UITextField) {
        isPasswordEquality()
    }
    
    @IBAction func finishRegBtn() {
        if let setUserDataVC = storyboard?.instantiateViewController(identifier: "SetUserDataVC") as? SetUserDataVC {
            userModel.password = passTF2.text
            setUserDataVC.userModel = userModel
            saveToken()
            navigationController?.pushViewController(setUserDataVC, animated: true)
        }
    }
    
    private func isPasswordEquality() {
        finishReg.isEnabled = (passTF.text == passTF2.text)
    }
    
    private func checkPasswordStrenght() {
            let passwordWithBigCh = NSPredicate(format: "SELF MATCHES %@ ",
                                                "^(?=.*[a-z])(?=.*[A-Z]).{6,}$")
            let passwordWithDigit = NSPredicate(format: "SELF MATCHES %@ ",
                                                "^(?=.*[a-z])(?=.*[0-9]).{6,}$")
            let passwordWithSpecialChar = NSPredicate(format: "SELF MATCHES %@ ",
                                                      "^(?=.*[a-z])(?=.*[$@$#!%*?&]).{6,}$")
            let passwordWithBigChAndDigit = NSPredicate(format: "SELF MATCHES %@ ",
                                                        "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$")
            let passwordWithBigChAndSpecialCh = NSPredicate(format: "SELF MATCHES %@ ",
                                                        "^(?=.*[a-z])(?=.*[A-Z])(?=.*[$@$#!%*?&]).{8,}$")
            let passwordWithDigitAndSpecialCh = NSPredicate(format: "SELF MATCHES %@ ",
                                                "^(?=.*[a-z])(?=.*[0-9])(?=.*[$@$#!%*?&]).{8,}$")
            let hardestPassword = NSPredicate(format: "SELF MATCHES %@ ",
                                                        "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$#!%*?&]).{10,}$")
            if let pass = passTF.text {
                switch pass {
                case _ where hardestPassword.evaluate(with: pass) :
                    verifView1.backgroundColor = .red
                    verifView2.backgroundColor = .orange
                    verifView3.backgroundColor = .yellow
                    verifView4.backgroundColor = .green
                case _ where passwordWithBigChAndDigit.evaluate(with: pass) || passwordWithBigChAndSpecialCh.evaluate(with: pass) || passwordWithDigitAndSpecialCh.evaluate(with: pass) :
                    verifView1.backgroundColor = .red
                    verifView2.backgroundColor = .orange
                    verifView3.backgroundColor = .yellow
                    verifView4.backgroundColor = .lightGray
                case _ where passwordWithDigit.evaluate(with: pass) || passwordWithBigCh.evaluate(with: pass) || passwordWithSpecialChar.evaluate(with: pass) :

                    verifView1.backgroundColor = .red
                    verifView2.backgroundColor = .orange
                    verifView3.backgroundColor = .lightGray
                    verifView4.backgroundColor = .lightGray
                case _ where pass.count >= 6 :
                    verifView1.backgroundColor = .red
                    verifView2.backgroundColor = .lightGray
                    verifView3.backgroundColor = .lightGray
                    verifView4.backgroundColor = .lightGray
                default:
                    verifView1.backgroundColor = .lightGray
                    verifView2.backgroundColor = .lightGray
                    verifView3.backgroundColor = .lightGray
                    verifView4.backgroundColor = .lightGray
                }
            }
        }
    
    private func saveToken() {
        if switchToken.isOn {
            let date = Date()
            UserDefaults.standard.set(date, forKey: "date")
        }
    }
}
