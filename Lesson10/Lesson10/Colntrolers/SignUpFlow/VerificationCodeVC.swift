//
//  VerificationCodeVC.swift
//  Lesson10
//
//  Created by Martynov Evgeny on 8.02.21.
//

import UIKit

class VerificationCodeVC: UIViewController {
    
    @IBOutlet weak var captchaImageView: UIImageView!
    
    @IBOutlet weak var codeTF: UITextField!
    
    
    var userModel: UserModel = UserModel()
    let captchaDict : [String : UIImage?] = ["swift"  : UIImage(named: "captcha1"),
                                             "artem"  : UIImage(named: "captcha2"),
                                             "power"  : UIImage(named: "captcha3"),
                                             "asdfs"  : UIImage(named: "captcha4"),
                                             "fjhdfg" : UIImage(named: "captcha5")]
    var currentKey : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        currentKey = radnomKey()
        captchaImageView.image = captchaDict[currentKey]!
    }
    
    
    @IBAction func codeTFChanged(_ sender: UITextField) {
        if sender.text == currentKey {
            if let setPasswordVC = storyboard?.instantiateViewController(identifier: "SetPasswordVC") as? SetPasswordVC {
                setPasswordVC.userModel = userModel
                navigationController?.pushViewController(setPasswordVC, animated: true)
            }
        }
    }
    
    @IBAction func captchaBtn() {
        var status = true
        repeat {
            let newKey =  radnomKey()
            if newKey != currentKey {
                captchaImageView.image = captchaDict[newKey]!
                currentKey = newKey
                status = false
            }
        } while status
    }
    
    private func radnomKey() -> String {
        let index = Int.random(in: 0..<captchaDict.count)
        let key = Array(captchaDict.keys)[index]
        return key
    }
}
