//
//  SetUserDataVC.swift
//  Lesson10
//
//  Created by Martynov Evgeny on 8.02.21.
//

import UIKit

class SetUserDataVC: UIViewController {
    
    
    @IBOutlet weak var maleOrFemaleOutlet: UISegmentedControl!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var surnameTF: UITextField!
    @IBOutlet weak var smokeSwitch: UISwitch!
    @IBOutlet weak var plusOrMinusOutlet: UIStepper!
    @IBOutlet weak var quantityKids: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var welcomeButton: UIButton!
    
    
    
    var userModel: UserModel = UserModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        welcomeButton.isEnabled = false
        }
    
   
    
    @IBAction func maleFemaleAction() {
        if maleOrFemaleOutlet.selectedSegmentIndex == 0 {
            userModel.addInfo?.gender = .male
        } else if maleOrFemaleOutlet.selectedSegmentIndex == 2 {
            userModel.addInfo?.gender = .female
        }
    }
    
    @IBAction func nameTFAction() {
        btn()
        if nameTF.text != nil && !nameTF.text!.isEmpty {
            userModel.name = nameTF.text
        
        }
            
    }
    
    
    
    @IBAction func surnameTFAct() {
        btn()
        if surnameTF.text != nil && !surnameTF.text!.isEmpty {
            userModel.surname = surnameTF.text
            
        }
        }
    
    
    
    @IBAction func switchAct() {
        if smokeSwitch.isOn {
            userModel.addInfo?.isSmoking = true
        } else if !smokeSwitch.isOn {
            userModel.addInfo?.isSmoking = false
        }
        }
    
    
    @IBAction func stepperAct() {
       quantityKids.text = String(Int(plusOrMinusOutlet.value))
        userModel.addInfo?.numOfChildrens = quantityKids.text
            
        }
    
    @IBAction func datePickerAct() {
        datePicker.datePickerMode = UIDatePicker.Mode.date
        let dateFormatter = DateFormatter()
              dateFormatter.dateFormat = "dd MMM yyyy"
        let selectedDate = dateFormatter.string(from: datePicker.date)
        userModel.addInfo?.dateOfBirth = selectedDate
    }
    
    @IBAction func buttomWelcome() {
        btn()
        UserDefaults.standard.set(try? PropertyListEncoder().encode(userModel), forKey:"user")
        
        let mainStoryboard = UIStoryboard(name: "MainApp", bundle: nil)
        let newVC = storyboard?.instantiateViewController(withIdentifier: "AppVC") as! AppVC; navigationController?.pushViewController(newVC, animated: true) 
        }
        
    
    


private func btn() {
    if nameTF.text != nil && !nameTF.text!.isEmpty && surnameTF.text != nil && !surnameTF.text!.isEmpty {
        welcomeButton.isEnabled = true
    } else if nameTF.text!.isEmpty || surnameTF.text!.isEmpty {
        welcomeButton.isEnabled = false
    }
}
}
