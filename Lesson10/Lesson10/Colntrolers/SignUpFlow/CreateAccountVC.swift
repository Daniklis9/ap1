//
//  CreateAccountVC.swift
//  Lesson10
//
//  Created by Martynov Evgeny on 8.02.21.
//

import UIKit

class CreateAccountVC: UIViewController {

    @IBOutlet weak var emailTFText: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var signUpBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func emailTFChanged(_ sender: UITextField) {
        signUpBtn.isEnabled = isValidEmail(sender.text ?? "")
    }
    
    @IBAction func beginEmailTF(_ sender: UITextField) {
        errorLabel.isHidden = true
    }
    
    @IBAction func didEndOnExitEmaiTF(_ sender: UITextField) {
        errorLabel.isHidden = isValidEmail(sender.text ?? "")
    }
    
    @IBAction func signUpBtnAction() {
        if let verificationVC = storyboard?.instantiateViewController(identifier: "VerificationCodeVC") as? VerificationCodeVC {
            verificationVC.userModel.email = emailTFText.text
            navigationController?.pushViewController(verificationVC, animated: true)
        }
    }
    
    private func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }

}
