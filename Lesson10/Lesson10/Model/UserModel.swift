//
//  UserModel.swift
//  Lesson10
//
//  Created by Martynov Evgeny on 3.02.21.
//

import Foundation

enum Genders: String, Codable {
    case male
    case female
}

struct AdditionalInfo : Codable {
    var gender         : Genders?
    var dateOfBirth    : String?
    var isSmoking      : Bool?
    var isVegaterian   : Bool?
    var numOfChildrens : String?
}

struct UserModel : Codable {
    var email    : String?
    var password : String?
    var name     : String?
    var surname  : String?
    var addInfo  : AdditionalInfo?
}
